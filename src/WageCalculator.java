import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;


public class WageCalculator extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// 1. Create & configure user interface controls
		
		// name
		Label nameLabel = new Label("Enter Name");
		TextField nameTextBox = new TextField();
		
		// hours worked
		Label hoursWorkedLabel = new Label("Enter Hours Worked");
		TextField hoursWorkedTextBox = new TextField();
		
		// hourly wage
		Label wageLabel = new Label("Enter Hourly Wage");
		TextField wageTextBox = new TextField();
        Label result=new Label();
		// calculate button
		Button btn = new Button();
		btn.setText("Calculate!");

		// Add event handler for button
		btn.setOnAction(new EventHandler<ActionEvent>() {
		    @Override
		    public void handle(ActionEvent e) {
		        // Logic for what should happen when you push button
		    		System.out.println("BUTTON PRESSED!");
		    		String name = nameTextBox.getText();
		    		double hours=Double.parseDouble(hoursWorkedTextBox.getText());
		    		double wage1=Double.parseDouble(wageTextBox.getText());
		    		double wage=hours*wage1;
		    		System.out.println("Total Wage of:" +name+" is "+wage);
		    		result.setText(name + " earns $ " +wage );

		    }
		});
		
		// results
		
		// 2. Make a layout manager
		VBox root = new VBox();
		
		// 3. Add controls to the layout manager
		root.getChildren().add(nameLabel);
		root.getChildren().add(nameTextBox);
		root.getChildren().add(hoursWorkedLabel);
		root.getChildren().add(hoursWorkedTextBox);
		root.getChildren().add(wageLabel);
		root.getChildren().add(wageTextBox);
		root.getChildren().add(btn);
		root.getChildren().add(result);
		
		// 4. Add layout manager to scene
		// 5. Add scene to a stage
		primaryStage.setScene(new Scene(root, 250, 300));
		
		// 6. Show the app
		primaryStage.show();
	}
}
